
# confluence-attachment-stats
## Summary
This repository hosts python scripts to create a statistics of attachment usage of Confluence Server by connecting to the database. It only supports PostgreSQL database at this moment.

## Installation
Just clone the repository

## Usage
`confluence-attachment-stats-psql.py -h host [-p port] -d dbname -U username [-W] [--psql]`

Similar to _psql_ command, _PGPASSWORD_ environmental variable is used for password as well as _-W_ option prompts for a password before connecting to a database.

if _--psql_ option is added, the script runs without psycopg2. If the options is not added, the script tries to import psycopg2. Please run `pip install psycopg2` if the library is not available.

## Output
It shows attachment usage statistics such as:
* number of attachments and size *in total*
* number of attachments and size of *latest version vs previous versions*
* number of attachments and size of *existing files vs trashed files*
* number of attachments and size *by created month*
* number of attachments and size *by mega bytes*

This is the sample output:
```
total - num:5,846 size:26,924,454,595

latest version - num:5,840 size:26,896,805,431
previous versions - num:6 size:27,649,164

existing files - num:5,844 size:26,919,033,049
trashed files - num:2 size:5,421,546

stats by month:
  2016-05 - num:5,844 size:26,919,033,049
  2016-04 - num:2 size:5,421,546

stats by mega bytes:
  ~5MB - num:1 size:4,602,692
  ~2MB - num:1 size:1,608,458
  ~1MB - num:7 size:4,900,622
```
