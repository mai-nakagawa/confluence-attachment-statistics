import os
import sys
import getpass
import getopt
import locale
from datetime import datetime

dbhost = None
dbport = 5432
dbname = None
dbuser = None
dbpass = None
usePsqlCommand = False

locale.setlocale(locale.LC_ALL, 'en_US')

sizeById = {}
sizeByMonth = {}
sizeBySizeRange = {}
numByMonth = {}
numBySizeRange = {}

totalNum = 0
totalSize = 0
latestNum = 0
latestSize = 0
prevNum = 0
prevSize = 0
existingNum = 0
existingSize = 0
trashNum = 0
trashSize = 0

def showUsage(self, writer):
    writer.write("usage: [PGPASSWORD=password] confluence-attachment-stats-psql.py -h host [-p port] -d dbname -U username [-W] [--psql]" + os.linesep)

options, args = getopt.getopt(sys.argv[1:], 'h:p:d:U:W', ['host=', 'port=', 'dbname=', 'username=', 'password', 'help', 'psql'])
for opt, arg in options:
    if opt in ('-h', '--host'):
        dbhost = arg
    elif opt in ('-p', '--port'):
        dbport = int(arg)
    elif opt in ('-d', '--dbname'):
        dbname = arg
    elif opt in ('-U', '--username'):
        dbuser = arg
    elif opt in ('-W', 'password'):
        dbpass = getpass.getpass()
    elif opt in ('--help'):
        showUsage(sys.stdout)
        sys.exit(0)
    elif opt in ('--psql'):
        usePsqlCommand = True

if os.environ.has_key('PGPASSWORD'):
    dbpass = os.environ['PGPASSWORD']

if not dbhost or not dbname or not dbuser:
    showUsage(sys.stderr)
    sys.stderr.write("error: too few arguments" + os.linesep)
    sys.exit(1)

def getSqlResultsByPsycopg2(sqlQuery):
    import psycopg2
    if dbpass:
        conn = psycopg2.connect("host=%s port=%d dbname=%s user=%s password=%s" % (dbhost, dbport, dbname, dbuser, dbpass))
    else:
        conn = psycopg2.connect("host=%s port=%s dbname=%s user=%s" % (dbhost, dbport, dbname, dbuser))

    cur = conn.cursor()
    cur.execute(sqlQuery)

    rows = []
    for row in cur:
        rows.append(row)
    cur.close()
    conn.close()
    return rows

def getSqlResultsByPsqlCommand(sqlQuery):
    import subprocess
    if dbpass:
        p = subprocess.Popen("PGPASSWORD=%s psql -t -A -F , -h %s -d %s -U %s -c \"%s\"" % (dbpass, dbhost, dbname, dbuser, sqlQuery), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    else:
        p = subprocess.Popen("psql -t -A -F , -h %s -d %s -U %s -c \"%s\"" % (dbhost, dbname, dbuser, sqlQuery), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    lines = []
    for line in stdout.rstrip().split("\n"):
        lines.append(line.rstrip().split(","))
    return lines

if usePsqlCommand:
    rows = getSqlResultsByPsqlCommand("SELECT longval,contentid FROM contentproperties WHERE propertyname = 'FILESIZE'")
else:
    rows = getSqlResultsByPsycopg2("SELECT longval,contentid FROM contentproperties WHERE propertyname = 'FILESIZE'")
for row in rows:
    size = row[0]
    contentId = row[1]
    sizeById[contentId] = int(size)

if usePsqlCommand:
    rows = getSqlResultsByPsqlCommand("SELECT contentid,creationdate,prevver,content_status FROM content WHERE contenttype = 'ATTACHMENT'")
else:
    rows = getSqlResultsByPsycopg2("SELECT contentid,creationdate,prevver,content_status FROM content WHERE contenttype = 'ATTACHMENT'")
for row in rows:
    contentId = row[0]
    creationdate = row[1]
    prevver = row[2]
    content_status = row[3]
    size = sizeById[contentId]
    sizeRange = size / 1024 / 1024 + 1

    if (isinstance(creationdate, str)):
        creationdate = datetime.strptime(creationdate.split(".")[0], "%Y-%m-%d %H:%M:%S")

    yearMonth = "%04d-%02d" % (creationdate.year, creationdate.month)
    if sizeByMonth.has_key(yearMonth):
        sizeByMonth[yearMonth] += size
    else:
        sizeByMonth[yearMonth] = size
    if numByMonth.has_key(yearMonth):
        numByMonth[yearMonth] += 1
    else:
        numByMonth[yearMonth] = 1

    totalNum += 1
    totalSize += size
    if prevver:
        prevNum += 1
        prevSize += size
    else:
        latestNum += 1
        latestSize += size
    if content_status == "current":
        existingNum += 1
        existingSize += size
    else:
        trashNum += 1
        trashSize += size

    if sizeBySizeRange.has_key(sizeRange):
        sizeBySizeRange[sizeRange] += size
    else:
        sizeBySizeRange[sizeRange] = size
    if numBySizeRange.has_key(sizeRange):
        numBySizeRange[sizeRange] += 1
    else:
        numBySizeRange[sizeRange] = 1

print "total - num:%s size:%s" % (locale.format("%d", totalNum, grouping=True), locale.format("%d", totalSize, grouping=True))
print
print "latest version - num:%s size:%s" % (locale.format("%d", latestNum, grouping=True), locale.format("%d", latestSize, grouping=True))
print "previous versions - num:%s size:%s" % (locale.format("%d", prevNum, grouping=True), locale.format("%d", prevSize, grouping=True))
print
print "existing files - num:%s size:%s" % (locale.format("%d", existingNum, grouping=True), locale.format("%d", existingSize, grouping=True))
print "trashed files -  num:%s size:%s" % (locale.format("%d", trashNum, grouping=True), locale.format("%d", trashSize, grouping=True))
print
print "stats by month:"
for yearMonth in sorted(sizeByMonth.keys(), reverse=True):
    print "\t%s - num:%s size:%s" % (yearMonth, locale.format("%d", numByMonth[yearMonth], grouping=True), locale.format("%d", sizeByMonth[yearMonth], grouping=True))
print
print "stats by sizeRange:"
for sizeRange in sorted(sizeBySizeRange.keys(), reverse=True):
    print "\t~%sMB - num:%s size:%s" % (sizeRange, locale.format("%d", numBySizeRange[sizeRange], grouping=True), locale.format("%d", sizeBySizeRange[sizeRange], grouping=True))
